<?php
/*
   This file is part of Tabocloud
   Copyright (C) 2020  Moritz Strohm

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


require_once(__DIR__ . '/../app/models/Bookmark.class.php');
require_once(__DIR__ . '/../app/models/Tag.class.php');



//Check for URL parameters:
$param_string = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
$params = [];
if ($param_string) {
    parse_str($param_string, $params);
}

$attributes = ['tags' => Tag::getAllTags()];
if (array_key_exists('tag', $params)) {
    $tag = mb_strtolower($params['tag']);
    $attributes['page_title'] = sprintf('Bookmarks tagged with %s', $tag);
    $attributes['bookmarks'] = Bookmark::findByTag($tag);
} else {
    $attributes['page_title'] = 'Latest bookmarks';
    $attributes['bookmarks'] = Bookmark::getLatest();
}
$app = Tabocloud::get();
$app->renderTemplate('main', $attributes);

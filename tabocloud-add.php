<?php


require_once(__DIR__ . '/app/models/Bookmark.class.php');
require_once(__DIR__ . '/app/helpers/BookmarkHelper.class.php');


$url = trim($argv[1]);
if (!$url) {
    echo "USAGE: tabocloud-add.php URL [TAGS] [WEIGHT]";
}

$untrimmed_tags = explode(',', @$argv[2]);
$tag_strings = [];
foreach ($untrimmed_tags as $ut) {
    $t = trim($ut);
    if ($t) {
        $tag_strings[] = $t;
    }
}
if (!$tag_strings) {
    echo "You did not provide any tags. If you want to do this now, enter them separated by comma. Otherwise just press enter.\n";
    $untrimmed_tags = explode(',', fgets(STDIN));
    foreach ($untrimmed_tags as $ut) {
        $t = trim($ut);
        if ($t) {
            $tag_strings[] = mb_strtolower($t);
        }
    }
}

$weight = intval(@$argv[3]);
if (!$weight && ($weight !== 0)) {
    echo "You did not provide a weight. You can do this now. Otherwise just press enter.\n";
    $weight = intval(trim(fgets(STDIN)));
}

$metadata = BookmarkHelper::getMetadataFromUrl($url);

if (@!$metadata['title']) {
    echo "The title could not be retrieved. You can enter it manually now. Otherwise just press enter.\n";
    $metadata['title'] = trim(fgets(STDIN));
}

$full_title = '';
if (@$metadata['site_name']) {
    $full_title = sprintf('%1$s: %2$s', $metadata['site_name'], $metadata['title']);
} else {
    $full_title = $metadata['title'] ?: $url;
}

//Add the new bookmark:
$b = new Bookmark();
$b->url = $url;
$b->title = $full_title;
$b->description = @$metadata['description'];
$b->weight = $weight;
$b->setTags($tag_strings);
if ($b->store()) {
    echo "Bookmark stored!\n";
} else {
    echo "Error while storing bookmark!\n";
    return 1;
}

#!/bin/bash

if [ "$1" = 'demo' ]
then
    echo "Setting up the demo database!"
    sqlite3 tabocloud.db < tabocloud-demo.sql
else
    sqlite3 tabocloud.db < tabocloud.sql
fi

cp htconfig.ini.dist .htconfig.ini

<!DOCTYPE html>
<html>
    <head>
        <title><?= htmlspecialchars($page_name) ?>: <?= htmlspecialchars($page_title) ?></title>
        <link rel="stylesheet" type="text/css" href="./style.css">
    </head>
    <body>
        <nav>
            <h1><a href="./"><?= htmlspecialchars($page_name) ?></a></h1>
        </nav>
        <?php if (@$error_message) : ?>
            <section class="error">
                <?= htmlspecialchars($error_message) ?>
            </section>
        <?php else : ?>
            <div class="main_box">
                <aside>
                    <h2>Tags</h2>
                    <?php if (@$tags) : ?>
                        <?php foreach ($tags as $tag) : ?>
                            <a href="./?tag=<?= htmlspecialchars($tag->name) ?>"
                               class="<?= htmlspecialchars($tag->getSizeClass()) ?>">
                                #<?= htmlspecialchars($tag->name) ?>
                            </a>
                        <?php endforeach ?>
                    <?php else : ?>
                        No tags available!
                    <?php endif ?>
                </aside>
                <main>
                    <h2><?= @htmlspecialchars($page_title) ?></h2>
                    <section class="content">
                        <?= @$content ?>
                    </section>
                </main>
            </div>
            <footer>
            </footer>
        <?php endif ?>
    </body>
</html>

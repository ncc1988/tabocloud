<?php if ($bookmarks) : ?>
    <ul class="bookmarks">
        <?php foreach ($bookmarks as $bookmark) : ?>
            <li class="bookmark">
                <a href="<?= htmlspecialchars($bookmark->url) ?>">
                    <?= htmlspecialchars($bookmark->title) ?>
                </a>
                <div class="description">
                    <?= htmlspecialchars($bookmark->description) ?>
                </div>
                <div class="url">
                    <?= htmlspecialchars($bookmark->url) ?>
                </div>
                <?php if (@$bookmark->tags) : ?>
                    <div class="tags">
                        <?php foreach ($bookmark->tags as $tag) : ?>
                            #<?= htmlspecialchars($tag->name) ?>
                        <?php endforeach ?>
                    </div>
                <?php endif ?>
            </li>
        <?php endforeach ?>
    </section>
<?php else : ?>
    <p class="info">
        No bookmarks available!
    </p>
<?php endif ?>

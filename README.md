# Tabocloud

Tabocloud is a little PHP web application that lets you share tagged bookmarks (URLs) with the world. Tabocloud is an abbreviation for tagged bookmark cloud.

A demo instance is available at: https://demo.der.moe/tabocloud/


## Installation

- Download the source (clone this repository).
- Make the "public" folder accessible for your web server.
- Make sure your web server runs PHP 7 with the dom, sqlite3 and mbstring extensions.
- Run setup.sh. It will set up the database (tabocloud.db) and copy the default configuration in .htconfig.ini. If this script is run with the "demo" parameter (setup.sh demo), it will setup the demo database instead of an empty database.

## Adding a link

At the moment this can only be done on the command line using the tabocloud-add.php script. You must at least pass an URL as first parameter.

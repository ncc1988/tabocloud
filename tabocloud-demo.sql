PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE bookmarks(
       `id` INTEGER PRIMARY KEY AUTOINCREMENT,
       `url` VARCHAR(1024) UNIQUE NOT NULL,
       `title` VARCHAR(256) NOT NULL,
       `description` VARCHAR(1024) NULL,
       `weight` INTEGER NOT NULL DEFAULT '0',
       `mkdate` BIGINT NOT NULL
);
INSERT INTO bookmarks VALUES(1,'https://example.org','Example',NULL,0,1586649688);
INSERT INTO bookmarks VALUES(2,'https://codeberg.org','Codeberg',NULL,0,1586649710);
INSERT INTO bookmarks VALUES(3,'https://codeberg.org/ncc1988/tabocloud','Tabocloud source code on Codeberg',NULL,10,1586649763);
CREATE TABLE tags(
       `id` INTEGER PRIMARY KEY AUTOINCREMENT,
       `name` VARCHAR(128) UNIQUE NOT NULL,
       `mkdate` BIGINT NOT NULL
);
INSERT INTO tags VALUES(1,'demo',1586649688);
INSERT INTO tags VALUES(2,'example',1586649688);
INSERT INTO tags VALUES(3,'codeberg',1586649710);
INSERT INTO tags VALUES(4,'tabocloud',1586649763);
CREATE TABLE bookmark_tags(
       `bookmark_id` INTEGER,
       `tag_id` INTEGER,
       PRIMARY KEY (`bookmark_id`, `tag_id`)
       FOREIGN KEY (`bookmark_id`) REFERENCES `bookmarks`(`id`),
       FOREIGN KEY (`tag_id`) REFERENCES `tags`(`id`)
);
INSERT INTO bookmark_tags VALUES(1,1);
INSERT INTO bookmark_tags VALUES(1,2);
INSERT INTO bookmark_tags VALUES(2,1);
INSERT INTO bookmark_tags VALUES(2,3);
INSERT INTO bookmark_tags VALUES(3,1);
INSERT INTO bookmark_tags VALUES(3,3);
INSERT INTO bookmark_tags VALUES(3,4);
CREATE TABLE web_user(
       `id` INTEGER PRIMARY KEY AUTOINCREMENT,
       `username` VARCHAR(128) UNIQUE NOT NULL,
       `password` VARCHAR(256) NOT NULL,
       `mkdate` BIGINT NOT NULL
);
DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('bookmarks',3);
INSERT INTO sqlite_sequence VALUES('tags',4);
COMMIT;

<?php
/*
   This file is part of Tabocloud
   Copyright (C) 2020  Moritz Strohm

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


class BookmarkHelper
{
    public static function getMetadataFromUrl(string $url) : array
    {
        $dom = new DOMDocument();
        $dom->strictErrorChecking = false;
        libxml_use_internal_errors(true);
        $ok = $dom->loadHTML(file_get_contents($url));
        libxml_clear_errors();
        if (!$ok) {
            return [];
        }

        $metadata = [];

        //Check for open graph tags:
        $meta_tags = $dom->getElementsByTagName('meta');
        foreach ($meta_tags as $tag) {
            $property = $tag->attributes->getNamedItem('property');
            if (!$property) {
                continue;
            }
            $content = $tag->attributes->getNamedItem('content');
            if (!$content) {
                continue;
            }
            if ($property->value == 'og:site_name') {
                $metadata['site_name'] = $content->value;
            } elseif ($property->value == 'og:title') {
                $metadata['title'] = $content->value;
            } elseif ($property->value == 'og:description') {
                $metadata['description'] = $content->value;
            }
        }

        if (!@$metadata['title']) {
            //Get the title from the title tag as fallback solution.
            $title_tag = $dom->getElementsByTagName('title')[0];
            if ($title_tag) {
                $metadata['title'] = $title_tag->textContent;
            }
        }
        return $metadata;
    }
}

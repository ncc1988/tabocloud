<?php
/*
   This file is part of Tabocloud
   Copyright (C) 2020  Moritz Strohm

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


class Tabocloud
{
    //the static parts:

    protected static $instance = null;


    public static function get()
    {
        if (!(self::$instance instanceof Tabocloud)) {
            self::$instance = new Tabocloud();
        }
        return self::$instance;
    }


    //The non-static parts.


    protected $template_path = __DIR__ . '/../templates/';

    /**
     * The database handle.
     */
    protected $db = null;


    protected $config = [];


    protected function __construct()
    {
        $parsed_config = @parse_ini_file(__DIR__ . '/../.htconfig.ini');
        if ($parsed_config === false) {
            $this->halt(
                'Tabocloud is not configured! The configuration file .htconfig.ini cannot be read!'
            );
            return;
        }
        $this->config = $parsed_config;

        $this->db = new PDO('sqlite:' . __DIR__ . '/../' . $this->config['db_file']);
        if (!$this->db) {
            throw new Exception('Database connection could not be established!');
        }
    }


    public function getDb()
    {
        return ($this->db instanceof PDO) ? $this->db : null;
    }


    public static function getDbStatic()
    {
        if (!self::$instance) {
            self::$instance = new Tabocloud();
        }
        return self::$instance->getDb();
    }


    /**
     * Renders a template and outputs its content.
     */
    public function renderTemplate(string $name, array $attributes = [], $use_layout = true)
    {
        //Locate the template:
        if (!file_exists($this->template_path . $name . '.php')) {
            $this->halt(sprintf('Template "%s" not found!', $name));
            return;
        }

        $page = function(string $layout_file, array $attributes = [], $use_layout = true) {
            ob_start();
            extract($attributes);
            include $layout_file;
            return ob_get_clean();
        };

        $attributes['page_name'] = (
            array_key_exists('page_name', $this->config)
            ? $this->config['page_name']
            : 'Tabocloud'
        );

        $content = $page(
            $this->template_path . $name . '.php',
            $attributes
        );

        if ($use_layout) {
            $attributes['content'] = $content;

            $layout = function(string $layout_file, array $attributes = []) {
                ob_start();
                extract($attributes);
                include $layout_file;
                return ob_get_clean();
            };
            echo $layout($this->template_path . 'layout.php', $attributes);
        } else {
            echo $content;
        }
    }


    public function halt(string $error_message)
    {
        //Load only the layout:
        if (!file_exists($this->template_path . 'layout.php')) {
            die('Layout template not found!');
        }

        $halt_page = function(string $layout_file, array $attributes = []) {
            ob_start();
            extract($attributes);
            include $layout_file;
            return ob_get_clean();
        };

        echo $halt_page(
            $this->template_path . 'layout.php',
            [
                'error_message' => $error_message,
                'page_name' => (
                    array_key_exists('page_name', $this->config)
                    ? $this->config['page_name']
                    : 'Tabocloud'
                ),
                'page_title' => 'Error'
            ]
        );
    }
}

<?php
/*
   This file is part of Tabocloud
   Copyright (C) 2020  Moritz Strohm

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


require_once('Model.class.php');


class Tag extends Model
{
    public $id;
    public $name;
    public $usage;
    public $mkdate;


    public static function rowToObject($row)
    {
        $object = new Tag();
        $object->id = $row['id'];
        $object->name = $row['name'];
        $object->mkdate = $row['mkdate'];
        return $object;
    }


    public static function getAllTags()
    {
        $db = Tabocloud::getDbStatic();
        $count_result = $db->query("SELECT COUNT(id) FROM tags");
        $amount = $count_result->fetchColumn();
        if (!$amount) {
            //No tags available
            return [];
        }
        $result = $db->query(
            "SELECT tags.id as id, tags.name as name, tags.mkdate as mkdate,
            COUNT(bookmark_tags.tag_id) as usage
            FROM tags
            INNER JOIN bookmark_tags ON tags.id = bookmark_tags.tag_id
            GROUP BY name ORDER BY usage DESC"
        );
        $tags = [];
        $rows = $result->fetchAll() ?: [];
        foreach ($rows as $row) {
            $o = self::rowToObject($row);
            $o->usage = $row['usage'] / $amount;
            $tags[] = $o;
        }
        return $tags;
    }


    public function getSizeClass()
    {
        return 'small'; //STUB
    }
}

<?php
/*
   This file is part of Tabocloud
   Copyright (C) 2020  Moritz Strohm

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


require_once('Model.class.php');
require_once('Tag.class.php');


class Bookmark extends Model
{
    public $id;
    public $url;
    public $title;
    public $description;
    public $weight;
    public $mkdate;

    public $tags;

    protected static $db_relation = [
        'tags' => ['load' => 'loadTags', 'store' => 'storeTags']
    ];


    public static function rowToObject($row)
    {
        $object = new Bookmark();
        $object->id = $row['id'];
        $object->url = $row['url'];
        $object->title = $row['title'];
        $object->description = $row['description'];
        $object->weight = $row['weight'];
        $object->mkdate = $row['mkdate'];
        $object->loadTags();
        return $object;
    }


    public static function getLatest()
    {
        $db = Tabocloud::getDbStatic();
        $stmt = $db->prepare(
            "SELECT id, url, title, description, weight, mkdate FROM bookmarks ORDER BY mkdate DESC LIMIT 10"
        );

        $stmt->execute();

        $result = [];
        $rows = $stmt->fetchAll() ?: [];
        foreach ($rows as $row) {
            $result[] = self::rowToObject($row);
        }
        return $result;
    }


    public static function findByTag(string $tag)
    {
        $db = Tabocloud::getDbStatic();
        $stmt = $db->prepare(
            "SELECT id, url, title, description, weight, mkdate
            FROM bookmarks
            INNER JOIN bookmark_tags ON bookmarks.id = bookmark_tags.bookmark_id
            WHERE
            bookmark_tags.tag_id = (SELECT id FROM tags WHERE name = :tag)
            ORDER BY weight DESC, mkdate DESC"
        );

        $stmt->execute(['tag' => $tag]);

        $result = [];
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            $result[] = self::rowToObject($row);
        }
        return $result;
    }


    public function loadTags()
    {
        $stmt = $this->db->prepare(
            "SELECT id, name, mkdate FROM tags
            INNER JOIN bookmark_tags
            ON tags.id = bookmark_tags.tag_id
            WHERE bookmark_tags.bookmark_id = :id
            ORDER BY name ASC"
        );
        $stmt->execute(['id' => $this->id]);
        $tag_rows = $stmt->fetchAll();

        $tags = [];
        foreach ($tag_rows as $row) {
            $tags[] = Tag::rowToObject($row);
        }
        $this->tags = $tags;
    }


    public function storeTags()
    {
        if (!$this->tags) {
            return;
        }
        $get_id_stmt = $this->db->prepare(
            "SELECT id FROM tags WHERE name = :name"
        );
        $insert_stmt = $this->db->prepare(
            "INSERT INTO tags (name, mkdate)
            VALUES (:name, :mkdate)"
        );
        $link_stmt = $this->db->prepare(
            "INSERT INTO bookmark_tags (bookmark_id, tag_id)
            VALUES (:bookmark_id, :tag_id)"
        );

        $unlink_stmt_params = [
            'bookmark_id' => $this->id
        ];
        foreach ($this->tags as $tag) {
            $unlink_stmt_params[] = $tag->name;
        }
        $unlink_stmt = $this->db->prepare(
            "DELETE FROM bookmark_tags
            WHERE bookmark_id = ?
            AND tag_id IN (
                SELECT id FROM tags
                WHERE name IN ( " . str_repeat('?, ', count($unlink_stmt_params) - 1) . "? )
            )"
        );
        $unlink_stmt->execute($unlink_stmt_params);

        foreach ($this->tags as $tag) {
            if (!($tag instanceof Tag)) {
                echo "t1 ";
                continue;
            }
            $in_database = false;
            $get_id_stmt->execute(['name' => $tag->name]);
            $id = $get_id_stmt->fetchColumn();
            if ($id) {
                $tag->id = $id;
                $in_database = true;
            }
            if (!$in_database) {
                $tag->mkdate = time();
                $ok = $insert_stmt->execute(['name' => $tag->name, 'mkdate' => $tag->mkdate]);
                if (!$ok) {
                    throw new Exception('Error storing tag!');
                }
                $get_id_stmt->execute(['name' => $tag->name]);
                $id = $get_id_stmt->fetchColumn();
                $tag->id = $id;
            }
            $link_stmt->execute(['bookmark_id' => $this->id, 'tag_id' => $tag->id]);
        }
    }


    public function store()
    {
        $get_id_stmt = $this->db->prepare(
            "SELECT id FROM bookmarks WHERE url = :url"
        );
        $insert_stmt = $this->db->prepare(
            "INSERT INTO bookmarks (url, title, description, weight, mkdate)
            VALUES (:url, :title, :description, :weight, :mkdate)"
        );
        $in_database = false;
        if ($this->id) {
            $get_id_stmt->execute(['url' => $this->url]);
            $id = $get_id_stmt->fetchColumn();
            if ($id) {
                $in_database = true;
            }
        }
        if (!$in_database) {
            $this->mkdate = time();
            $ok = $insert_stmt->execute(
                [
                    'url' => $this->url,
                    'title' => $this->title,
                    'description' => $this->description,
                    'weight' => $this->weight,
                    'mkdate' => $this->mkdate
                ]
            );
            if (!$ok) {
                throw new Exception('Error storing bookmark!');
            }
            $ok = $get_id_stmt->execute(['url' => $this->url]);
            if (!$ok) {
                throw new Exception('Error querying the new ID!');
            }
            $id = $get_id_stmt->fetchColumn();
            if (!$id) {
                throw new Exception('Error getting new ID!');
            }
            $this->id = $id;
        }

        $this->storeTags();
        return true;
    }


    public function setTags(array $tag_strings = [])
    {
        $this->tags = [];
        foreach ($tag_strings as $tag_string) {
            $tag_name = mb_strtolower(trim($tag_string));
            if ($tag_name) {
                $tag = new Tag();
                $tag->name = $tag_name;
                $this->tags[] = $tag;
            }
        }
    }
}

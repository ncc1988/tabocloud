CREATE TABLE bookmarks(
       `id` INTEGER PRIMARY KEY AUTOINCREMENT,
       `url` VARCHAR(1024) UNIQUE NOT NULL,
       `title` VARCHAR(256) NOT NULL,
       `description` VARCHAR(1024) NULL,
       `weight` INTEGER NOT NULL DEFAULT '0',
       `mkdate` BIGINT NOT NULL
);


CREATE TABLE tags(
       `id` INTEGER PRIMARY KEY AUTOINCREMENT,
       `name` VARCHAR(128) UNIQUE NOT NULL,
       `mkdate` BIGINT NOT NULL
);


CREATE TABLE bookmark_tags(
       `bookmark_id` INTEGER,
       `tag_id` INTEGER,
       PRIMARY KEY (`bookmark_id`, `tag_id`)
       FOREIGN KEY (`bookmark_id`) REFERENCES `bookmarks`(`id`),
       FOREIGN KEY (`tag_id`) REFERENCES `tags`(`id`)
);


CREATE TABLE web_user(
       `id` INTEGER PRIMARY KEY AUTOINCREMENT,
       `username` VARCHAR(128) UNIQUE NOT NULL,
       `password` VARCHAR(256) NOT NULL,
       `mkdate` BIGINT NOT NULL
);
